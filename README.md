<div align="center">
  <img src="https://github.com/romulovalez/danchiano-applicant-test/raw/master/logo.gif" alt="Logo" width="643" height="431">
</div>

<div align="center">
  <h1>D'Anchiano applicant test</h1>
  <p>Add it to your platform the easy way!</p>
</div>

---

## 🚀 Installation

```shell
yarn add styled-components @romulovalez/da-applicant-test
```

```shell
npm install styled-components @romulovalez/da-applicant-test
```

In order to use it you will need to have access to the D'Anchiano API.

## 🖲 Usage

```jsx
import React from 'react'
import ApplicantTest from '@romulovalez/da-applicant-test'

const Example = () => (
  <ApplicantTest
    baseUrl="https://dev.danchiano.com"
    token="57f3fd937c4e0814ffa68c1a67f2e53d" />
)
```

## 🧬 Properties

| Property | Type   | Description            |
| -------- | ------ | ---------------------- |
| baseUrl  | String | D'Anchiano base url    |
| token    | String | Applicant access token |

## 🔑 Get applicant token
```
GET {baseUrl}/api/v1/users/{applicantId}/token
header = {
	'Authorization': 'Bearer {portalToken}',
	'Accept-Language': {lang},
}
```
| Property    | Type   | Description            |
| ----------- | ------ | ---------------------- |
| portalToken | String | Portal access token ([get it here](https://danchiano.com/api/doc/#autenticando-con-oauth-2-0)) |
| lang        | String | ISO 639-1 Language code |


This token will allow you to start an applicant's session on the frontend.
If it does not exist or if it is expired it generates a new token.
It expires after 24 hours.

You can force the generation of a new token by adding `?refresh` to the request.

## 🛠 Endpoints

If you want to make your own frontend this are the endpoints used:

### Answer the question 'people-in-charge'
```
POST {baseUrl}/api/v2/applicant/people-in-charge?token={token}
```
| Property | Type   | Description            |
| -------- | ------ | ---------------------- |
| value    | bool   | people at charge value |


### Get the current question of the test
```
GET {baseUrl}/api/v2/applicant/questions?token={token}
```

### Answer the current question
```
POST {baseUrl}/api/v2/applicant/answer/{answerId}?token={token}
```
| Property     | Type    | Description  |
| ------------ | ------- | ------------ |
| answerValue  | String  | answer (a,b) |


### Update the time elapsed in the test
```
PATCH {baseUrl}/api/v2/applicant/test-timer?token={token}
```
| Property  | Type    | Description     |
| --------- | ------- | --------------- |
| timer     | Number  | time in seconds |


## 📝 License

MIT