## 2.0.1 (February 18, 2020)

* Added regenerator-runtime for async / await support
* Added abort capability to AJAX requests

## 2.0.0 (February 17, 2020)

* Changed from people-at-charge to people-in-charge
* Changed from peopleAtCharge to peopleInCharge
* Fixed bug in request function
* Marked styled-components as peerDependencies
* Removed babel-plugin-external-helpers (seems is duplicating something with rollup-plugin-deps-external)

### Breaking changes
* Styled-components is now a peerDependency