import styled, { css, createGlobalStyle } from 'styled-components'

const fontMontserrat = 'Montserrat, sans-serif'

const styles = {}

styles.GlobalStyle = createGlobalStyle`
  html {
    /* Make texts equal in all browsers / OS */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeLegibility;
  }

  html {
    box-sizing:border-box;
  }

  *, *:before, *:after {
    box-sizing:inherit;
  }

  html, body, #root {
    height: 100%;
  }

  body {
    margin: 0;
  }
`
styles.Container = styled.div`
  background-color: #fafafa;
  height: 100%;
  display:flex;
  align-items: center;
`
styles.LoadingIcon = styled.img`
  margin: 0 auto;
`
styles.Box = styled.div`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  max-width: ${props => props.maxWidth};
  text-align: center;
  box-shadow: 0 10px 100px rgba(0, 0, 0, 0.07);
  border-radius: 8px;

  ${props => props.background && css`
    background: ${props.background};
  `};
  ${props => props.color && css`
    color: ${props.color};
  `};
`
styles.BoxText = styled.h3`
  font-family: ${fontMontserrat};
  font-size: 18px;
  margin-bottom: 0;
`
styles.BoxIcon = styled.img`
  height: 36px;
  width: 36px;
`

export default styles