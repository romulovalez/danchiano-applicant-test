import React, { useState, useEffect } from 'react'
import styles from './Test.styles'
import { request, prependZero } from './utils'
import logoIcon from './assets/logo.svg'
import timerIcon from './assets/timer.svg'

const i18n = {
  instructions: 'Instrucciones',
}

export default function Test({ baseUrl, token, setPage, setCompleteData }) {
  /**
   * State
   */
  const [disabled, setDisabled] = useState(false)
  const [timer, setTimer] = useState(0)
  const [question, setQuestion] = useState({
    id: 1,
    questions: [
      {
        index: 'a',
        icon: null,
        negative: false,
        text: '',
      },
      {
        index: 'b',
        icon: null,
        negative: false,
        text: '',
      },
    ],
  })

  /**
   * Functions
   */
  const getQuestion = async signal => {
    try {
      const result = await request(`${baseUrl}/api/v2/applicant/questions`, token, signal)
      setQuestion(result)
      setTimer(result.timer)

      // Initialize timer, every 15 seconds update it in server
      const intervalId = setInterval(() => {
        if (signal.aborted) {
          clearInterval(intervalId)
          return
        }

        setTimer(timer => {
          if (timer % 15 === 0)
            request(`${baseUrl}/api/v2/applicant/test-timer`, token, signal, 'put', { timer })
          return timer + 1
        })
      }, 1000)
    } catch (error) {
      if (error.message === 'User is not logged in') {
        setPage('error')
      } else if (error.message === 'peopleInCharge is not defined') {
        setCompleteData(false)
        setPage('welcome')
      }
    }
  }

  const sendAnswer = async answerValue => {
    setDisabled(true)
    try {
      const result = await request(`${baseUrl}/api/v2/applicant/answer/${question.id}`, token, null, 'post', { answerValue })
      setDisabled(false)
      setQuestion(result)
    } catch (error) {
      setDisabled(false)
      if (error.message === 'User is not logged in') {
        setPage('error')
      } else if (error.message === 'peopleInCharge is not defined') {
        setCompleteData(false)
        setPage('welcome')
      } else {
        // Answer out of sync
        window.location.reload()
      }
    }
  }

  /**
   * Effects
   */
  useEffect(() => {
    const abortController = new AbortController()
    const { signal } = abortController

    getQuestion(signal)

    return () => abortController.abort()
  }, [])

  /**
   * Variables
   */
  const min = parseInt(timer / 60)
  const sec = timer % 60
  const percentage = `${parseInt((question.id * 100) / 108)}%`
  const blankImage = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='

  /**
   * Render
   */
  return (
    <styles.Box maxWidth="1040px">
      <styles.Logo
        src={logoIcon}
        alt="logo" />
      <styles.GreenBackground>
        <styles.Timer>
          <styles.TimerIcon
            src={timerIcon}
            alt="timer icon" />
          <styles.TimerText>{`${prependZero(min)}:${prependZero(sec)}`}</styles.TimerText>
        </styles.Timer>
        <styles.ButtonContainer>
          {question.questions.map((elem, index) => (
            <styles.Button
              key={elem.index}
              type="button"
              name={`question-${elem.index}`}
              value="true"
              disabled={disabled}
              onClick={() => sendAnswer(elem.index)}>
              <styles.ButtonIconContainer
                negative={question.questions[index].negative}>
                <styles.ButtonIcon
                  src={question.questions[index].icon || blankImage}
                  alt="question icon" />
              </styles.ButtonIconContainer>
              <styles.ButtonText>
                {question.questions[index].text}
              </styles.ButtonText>
            </styles.Button>))}
        </styles.ButtonContainer>
        <styles.PercentageBarContainer>
          <styles.BarPlaceholder>
            <styles.Bar style={{ width: percentage }} />
          </styles.BarPlaceholder>
        </styles.PercentageBarContainer>
        <styles.Percentage>{percentage}</styles.Percentage>
      </styles.GreenBackground>
      <styles.Back onClick={() => setPage('welcome')}>
        {i18n.instructions}
      </styles.Back>
    </styles.Box>
  )
}