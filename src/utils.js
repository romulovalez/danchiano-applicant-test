export const request = async (endpoint, token, signal, method = null, body = null) => {
  const request = {
    headers: { cookies: document.cookie },
  }
  if (signal)
    request.signal = signal
  if (method) {
    request.method = method
    request.headers['Content-Type'] = 'application/json'
    if (body)
      request.body = JSON.stringify(body)
  }

  const response = await fetch(`${endpoint}${token ? `?token=${token}` : ''}`, request)
  const data = await response.json()

  if (!data.status)
    throw new Error(data.message)
  return data
}

export const prependZero = number => ((number < 10) ? `0${number}` : number)

export const stepsAnimation = async () => {
  function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
  }
  async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; ++index) {
      await callback(array[index], index, array) // eslint-disable-line no-await-in-loop
    }
  }

  const stepIcons = document.querySelectorAll('.step-icon')
  asyncForEach(stepIcons, async icon => {
    icon.animate([
      { transform: 'scale(1)' },
      { transform: 'scale(1.1)' },
      { transform: 'scale(1)' },
    ], {
      duration: 300,
      iterations: 1,
    })
    await delay(150)
  })
}