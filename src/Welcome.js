import React, { useState, useEffect } from 'react'
import styles from './Welcome.styles'
import { request, stepsAnimation } from './utils'
import logoIcon from './assets/logo.svg'
import step1Icon from './assets/step1.svg'
import step2Icon from './assets/step2.svg'
import step3Icon from './assets/step3.svg'
import step4MaleIcon from './assets/step4-male.svg'
import step4FemaleIcon from './assets/step4-female.svg'
import loadingWhiteIcon from './assets/loadingWhite.svg'

const i18n = {
  welcome: "Te damos la bienvenida al test D'Anchiano",
  step1: 'Tardarás unos 12 minutos en completarlo',
  step2: 'En cada par de opciones, elige la que mejor te represente',
  step3: 'Siempre deberás elegir una de las dos opciones',
  step4: 'Responde con sinceridad, no hay respuestas correctas o incorrectas',
  radioText: 'Necesitamos algunos datos más para personalizar el cuestionario de tu test:',
  withPeopleInCharge: 'He tenido personas a cargo',
  withoutPeopleInCharge: 'No he tenido personas a cargo',
  start: 'Comenzar',
  continue: 'Continuar',
}

/**
 * Component
 */
export default function Welcome({ baseUrl, token, sex, currentQuestionId, completeData, setCompleteData, setPage }) {
  /**
   * State
   */
  const [peopleInCharge, setpeopleInCharge] = useState(null)
  const [loading, setLoading] = useState(false)

  /**
   * Effects
   */
  useEffect(() => {
    stepsAnimation()
  }, [])

  /**
   * Functions
   */
  const buttonHandler = async () => {
    if (peopleInCharge != null) {
      try {
        setLoading(true)
        await request(`${baseUrl}/api/v2/applicant/people-in-charge`, token, null, 'post', { value: peopleInCharge })
        setLoading(false)
        setCompleteData(true)
        setPage('test')
      } catch (error) {
        setLoading(false)
        if (error.message === 'User is not logged in') {
          setPage('error')
        }
      }
    } else {
      setPage('test')
    }
  }

  /**
   * Render
   */
  return (
    <styles.Box maxWidth="650px">
      <styles.Logo
        src={logoIcon}
        alt="logo" />
      <styles.Text>{i18n.welcome}</styles.Text>
      <styles.Icons>
        <styles.IconContainer>
          <styles.Icon className="step-icon" src={step1Icon} alt="step1Icon" />
          <styles.IconText>{i18n.step1}</styles.IconText>
        </styles.IconContainer>
        <styles.IconContainer>
          <styles.Icon className="step-icon" src={step2Icon} alt="step2Icon" />
          <styles.IconText>{i18n.step2}</styles.IconText>
        </styles.IconContainer>
        <styles.IconContainer>
          <styles.Icon className="step-icon" src={step3Icon} alt="step3Icon" />
          <styles.IconText>{i18n.step3}</styles.IconText>
        </styles.IconContainer>
        <styles.IconContainer>
          {sex === 'male'
            ? <styles.Icon className="step-icon" src={step4MaleIcon} alt="step4MaleIcon" />
            : <styles.Icon className="step-icon" src={step4FemaleIcon} alt="step4FemaleIcon" />}
          <styles.IconText>{i18n.step4}</styles.IconText>
        </styles.IconContainer>
      </styles.Icons>

      {!completeData &&
        <>
          <styles.RadioText>{i18n.radioText}</styles.RadioText>
          <styles.RadioContainer>
            <styles.RadioInnerContainer>
              <styles.RadioButtonContainer>
                <styles.RadioButton
                  type="radio"
                  id="with-people-in-charge"
                  name="people-in-charge"
                  value="true"
                  onChange={e => setpeopleInCharge(e.target.value)} />
                <styles.RadioLabel htmlFor="with-people-in-charge">
                  {i18n.withPeopleInCharge}
                </styles.RadioLabel>
              </styles.RadioButtonContainer>
              <styles.RadioButtonContainer>
                <styles.RadioButton
                  type="radio"
                  id="without-people-in-charge"
                  name="people-in-charge"
                  value="false"
                  onChange={e => setpeopleInCharge(e.target.value)} />
                <styles.RadioLabel htmlFor="without-people-in-charge">
                  {i18n.withoutPeopleInCharge}
                </styles.RadioLabel>
              </styles.RadioButtonContainer>
            </styles.RadioInnerContainer>
          </styles.RadioContainer>
        </>}

      <styles.Button
        disabled={!completeData && peopleInCharge == null}
        onClick={buttonHandler}>
        {loading &&
          <styles.ButtonLoading
            src={loadingWhiteIcon}
            alt="loadingWhiteIcon" />}
        {currentQuestionId > 0 ? i18n.continue : i18n.start}
      </styles.Button>
    </styles.Box>
  )
}