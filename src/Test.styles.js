import styled, { css } from 'styled-components'

const colorGreen = '#3ca'
const colorGray = '#bebebe'
const colorPercentageBar = '#3b9'
const breakMedium = '768px'
const negationHeight = '7px'
const fontMontserrat = 'Montserrat, sans-serif'
const fontLato = 'Lato, sans-serif'

const styles = {}

styles.Box = styled.div`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  max-width: ${props => props.maxWidth};
  text-align: center;
  box-shadow: 0 10px 100px rgba(0, 0, 0, 0.07);
  border-radius: 8px;
`
styles.Logo = styled.img`
  height: 18px;
  margin-bottom: 15px;
`
styles.GreenBackground = styled.div`
  background: ${colorGreen};
  margin: 0 -20px;
`
styles.Timer = styled.div`
  color: #fff;
  padding: 15px 0 0;

  display: flex;
  justify-content: center;
  align-items: center;
`
styles.TimerIcon = styled.img`
  height: 22px;
  margin-right: 2px;
`
styles.TimerText = styled.span`
  font-family: ${fontLato};
  font-size: 15px;
`
styles.ButtonContainer = styled.div`
  display: flex;
  @media (max-width: ${breakMedium}) {
    flex-direction: column;
  }
`
styles.Button = styled.button`
  background: #fff;
  cursor: pointer;
  border: 0;
  outline: 0;

  display: flex;
  flex-direction: column;
  align-items: center;

  width: 50%;
  border-radius: 6px;
  margin: 20px;
  padding: 20px;
  @media (max-width: ${breakMedium}) {
    width: auto;
    flex-direction: row;
  }

  ${props => props.disabled && css`
    cursor: initial;
    pointer-events: none;
  `}
`
styles.ButtonIconContainer = styled.div`
  width: 78px;
  height: 78px;
  border-radius: 39px;
  margin: 0 auto 20px;
  @media (max-width: ${breakMedium}) {
    min-width: 50px;
    width: 50px;
    height: 50px;
    margin: 0 20px 0 0;
  }

  background: ${colorGray};

  /* Center ButtonIcon */
  display: flex;
  align-items: center;
  justify-content: center;
  
  /* Hide ButtonIcon drop-shadow */
  overflow: hidden;

  ${props => props.negative && css`
    position: relative;
    &:after {
      position: absolute;
      content: ' ';
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: linear-gradient(135deg,
        transparent calc(50% - (${negationHeight} / 2)),
        rgba(224,102,78,.8) calc(50% - (${negationHeight} / 2)),
        rgba(224,102,78,.8) calc(50% + (${negationHeight} / 2)),
        transparent calc(50% + (${negationHeight} / 2))
      );
    }
  `}
`
styles.ButtonIcon = styled.img`
  width: 65%;
  height: 65%;

  filter:
    drop-shadow(1px 1px 0px #ababab)
    drop-shadow(2px 2px 0px #ababab)
    drop-shadow(3px 3px 0px #ababab)
    drop-shadow(4px 4px 0px #ababab)
    drop-shadow(5px 5px 0px #ababab)
    drop-shadow(6px 6px 0px #ababab)
    drop-shadow(7px 7px 0px #ababab)
    drop-shadow(8px 8px 0px #ababab);
`
styles.ButtonText = styled.div`
  font-family: ${fontMontserrat};
  font-size: 18px;
  text-align: center;

  height: 4em;
  vertical-align: middle;
  @media (max-width: ${breakMedium}) {
    height: auto;
    text-align: initial;
  }
`
styles.PercentageBarContainer = styled.div`
  padding: 0 20px 15px;
`
styles.BarPlaceholder = styled.div`
  height: 8px;
  max-width: 465px;
  background: ${colorPercentageBar};
  margin: 0 auto;
  border-radius:4px;
`
styles.Bar = styled.div`
  height: 8px;
  border-radius: 4px;
  background: #fff;
  transition: width 150ms;
`

styles.Percentage = styled.div`
  color: #fff;
  font-family: ${fontLato};
  font-size: 15px;
  padding-bottom: 15px;
`

styles.Back = styled.button`
  cursor: pointer;

  border: 1px solid ${colorGreen};
  outline: 0;
  border-radius: 4px;

  background: #fff;
  color: ${colorGreen};
  text-decoration: none;
  font-family: ${fontMontserrat};
  font-weight: 500;
  font-size: 16px;

  padding: 8px 15px;
  margin: 20px 20px 0;
`

export default styles