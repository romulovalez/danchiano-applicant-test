import React, { useState, useEffect } from 'react'
import { request } from './utils'
import Welcome from './Welcome'
import Test from './Test'
import styles from './index.styles'
import loadingIcon from './assets/loading.svg'
import successIcon from './assets/success.svg'
import errorIcon from './assets/error.svg'
import 'regenerator-runtime/runtime'

const i18n = {
  notLogged: 'Sesión caducada',
  testCompleted: 'Test completado',
}

export default function App({ baseUrl, token }) {
  /**
   * State
   */
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState('welcome')
  const [completeData, setCompleteData] = useState(true)
  const [currentQuestionId, setCurrentQuestionId] = useState(null)

  /**
   * Functions
   */
  const getTestState = async () => {
    try {
      const result = await request(`${baseUrl}/api/v2/applicant/questions`, token)
      setLoading(false)
      setCurrentQuestionId(result.id)
    } catch (error) {
      setLoading(false)
      if (error.message === 'User is not logged in') {
        setPage('error')
      } else if (error.message === 'peopleInCharge is not defined') {
        setPage('welcome')
        setCompleteData(false)
      } else if (error.message === 'Test is already passed') {
        setPage('success')
      }
    }
  }

  /**
   * Effects
   */
  useEffect(() => {
    getTestState()
  }, [])

  /**
   * Render
   */
  return (
    <>
      <styles.GlobalStyle />
      <styles.Container>
        {loading ?
          <styles.LoadingIcon src={loadingIcon} alt="loadingIcon" />
        : page === 'error' ?
          <styles.Box
            maxWidth="240px"
            background="#e42"
            color="#fff">
            <styles.BoxIcon src={errorIcon} alt="errorIcon" />
            <styles.BoxText>{i18n.notLogged}</styles.BoxText>
          </styles.Box>
        : page === 'success' ?
          <styles.Box
            maxWidth="240px"
            background="#3ca"
            color="#fff">
            <styles.BoxIcon src={successIcon} alt="successIcon" />
            <styles.BoxText>{i18n.testCompleted}</styles.BoxText>
          </styles.Box>
        : page === 'welcome' ?
          <Welcome
            baseUrl={baseUrl}
            token={token}
            sex="female"
            currentQuestionId={currentQuestionId}
            completeData={completeData}
            setCompleteData={setCompleteData}
            setPage={setPage} />
        :
          <Test
            baseUrl={baseUrl}
            token={token}
            setPage={setPage}
            setCompleteData={setCompleteData} />}
      </styles.Container>
    </>
  )
}