import styled, { css } from 'styled-components'

const colorGreen = '#3ca'
const breakMedium = '768px'
const fontMontserrat = 'Montserrat, sans-serif'
const radioCheckedColor = colorGreen
const radioSize = 18
const radioCheckedSize = 10

const styles = {}

styles.Box = styled.div`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  max-width: ${props => props.maxWidth};
  text-align: center;
  box-shadow: 0 10px 100px rgba(0, 0, 0, 0.07);
  border-radius: 8px;
`
styles.Logo = styled.img`
  height: 18px;
  margin-bottom: 15px;
`
styles.Text = styled.h3`
  margin: 20px;

  color: #333;
  font-size: 15px;
  font-weight: 700;
  font-family: ${fontMontserrat};
  text-transform: uppercase;
`
styles.Icons = styled.div`
  display: flex;
  @media (max-width: ${breakMedium}) {
    flex-direction: column;
  }
`
styles.IconContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
styles.Icon = styled.img`
  width: 100%;
  height: 170px;
`
styles.IconText = styled.span`
  font-family: ${fontMontserrat};
  padding: 0 10px;
  font-size: 14px;
`
styles.RadioText = styled.p`
  font-family: ${fontMontserrat};
  font-weight: 500;

  padding: 50px 20px 15px;
`
styles.RadioContainer = styled.div`
  display: inline-block;
  align-items: center;

  background: #33ccaa0f;
  padding: 15px 25px 0px 15px;
  border-radius: 10px;
`
styles.RadioInnerContainer = styled.div`
  text-align: initial;
`
styles.RadioButtonContainer = styled.div`
  padding-bottom: 15px;
`
styles.RadioButton = styled.input`
  display: none;
  &:checked + label:before {
    border-color: ${radioCheckedColor};
  }
  &:checked + label:after {
    transform: scale(1);
  }
`
styles.RadioLabel = styled.label`
  color: #1e7a66ba;
  font-family: ${fontMontserrat};
  font-weight: 500;
  line-height: ${radioSize + 2}px;

  position: relative;
  padding: 8px 0 8px ${radioSize + 8}px;
  cursor: pointer;
  outline: none;

  &:before, &:after {
    position: absolute;            
    content: '';  
    border-radius: 50%;
    transition:
      transform  0.3s ease,
      border-color 0.3s ease;
  }
  &:before {
    left: 0;
    width: ${radioSize}px;
    height: ${radioSize}px;
    border: 1px solid #1e7a6659;
  }
  &:after {
    left: 0;
    width: ${radioCheckedSize}px;
    height: ${radioCheckedSize}px;
    margin: ${radioSize / 2 - radioCheckedSize / 2}px;
    
    transform: scale(0);
    background: ${radioCheckedColor};
  }
`
styles.Button = styled.button`
  border: 1px solid ${colorGreen};
  outline: 0;
  border-radius: 4px;

  color: #fff;
  background: ${colorGreen};
  text-decoration: none;
  font-family: ${fontMontserrat};
  font-weight: 700;
  font-size: 16px;

  display: flex;
  align-items: center;
  min-height: 34px;
  padding: 6px 12px;

  margin: 35px auto 0;

  ${props => props.disabled && css`
    cursor: initial;
    pointer-events: none;
    opacity: 0.2;
  `}

  ${props => !props.disabled && css`
    cursor: pointer;
    &:hover {
      opacity: 0.8;
    }
  `}
`
styles.ButtonLoading = styled.img`
  width: auto;
  height: 20px;
  margin-right: 6px;
`

export default styles