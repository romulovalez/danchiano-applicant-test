import babel from 'rollup-plugin-babel'
import external from 'rollup-plugin-peer-deps-external'
// import resolve from '@rollup/plugin-node-resolve'
// import commonjs from '@rollup/plugin-commonjs'
import url from '@rollup/plugin-url'
import pkg from './package.json'

export default {
  input: 'src/index.js',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true,
    },
  ],
  external: ['regenerator-runtime/runtime'],
  plugins: [
    external(),
    url(),
    babel({
      exclude: 'node_modules/**',
    }),
    // resolve(),
    // commonjs(),
  ],
}